from numpy import *
import numpy

from dayBase import DayBase

class Day5(DayBase):
    def __init__(self):
        super().__init__("5")

    def test(self):       
        return self.__test1() and self.__test2()

    def __test1(self):
        with open('day5/input/test') as test_input_file:
            testInput = test_input_file.readlines()

        with open('day5/input/test_result') as test_input_result_file:
            testInputResult = test_input_result_file.readlines()
        
        result = True

        testResult = self.__solve1(testInput, 10)
        if testResult == int(testInputResult[0]):
            print("Test1: SUCCESS, test result: ", testResult, ", which should be: ", testInputResult)
        else:
            print("Test1: FAILED, test result: ", testResult, ", which should be: ", testInputResult)
            result = False
        return result

    def __test2(self):
        with open('day5/input/test') as test_input_file:
            testInput = test_input_file.readlines()

        with open('day5/input/test_result2') as test_input_result_file:
            testInputResult = test_input_result_file.readlines()
        
        result = True

        testResult = self.__solve2(testInput, 10)
        if testResult == int(testInputResult[0]):
            print("Test2: SUCCESS, test result: ", testResult, ", which should be: ", testInputResult)
        else:
            print("Test2: FAILED, test result: ", testResult, ", which should be: ", testInputResult)
            result = False
        return result

    def solve1(self):
        with open('day5/input/input') as input_file:
            input = input_file.readlines()
        return self.__solve1(input, 1000)

    def solve2(self):
        with open('day5/input/input') as input_file:
            input = input_file.readlines()
        return self.__solve2(input, 1000)

    def __solve1(self, input, size):
        diagram = [[0 for i in range(size)] for j in range(size)]

        for line in input:
            positions = line.strip().split(" -> ")
            start = list(map(int, positions[0].split(",")))
            target = list(map(int, positions[1].split(",")))

            xRange = None
            xDiff = target[0] - start[0]
            if xDiff == 0:
                xRange = [start[0]]
            else:
                xRange = range(start[0], target[0] + numpy.sign(xDiff), numpy.sign(xDiff))

            yRange = None
            yDiff = target[1] - start[1]
            if yDiff == 0:
                yRange = [start[1]]
            else:
                yRange = range(start[1], target[1] + numpy.sign(yDiff), numpy.sign(yDiff))

            if numpy.abs(xDiff) > 0 and numpy.abs(yDiff) > 0:
                continue

            for x in xRange:
                for y in yRange:
                    diagram[y][x] += 1

        howManyIsMoreThanOne = 0
        for col in diagram:
            for num in col:
                if num > 1:
                    howManyIsMoreThanOne += 1
        return howManyIsMoreThanOne
            

    def __solve2(self, input, size):
        diagram = [[0 for i in range(size)] for j in range(size)]

        for line in input:
            positions = line.strip().split(" -> ")
            start = list(map(int, positions[0].split(",")))
            target = list(map(int, positions[1].split(",")))
            
            xDiff = target[0] - start[0]
            yDiff = target[1] - start[1]

            if numpy.abs(xDiff) > 0 and numpy.abs(yDiff) > 0:
                # Diagonal line
                ran = [[0 for i in range(2)] for j in range(numpy.abs(xDiff) + 1)]
                for index in range(numpy.abs(xDiff) + 1):
                    ran[index] = [start[0] + index * numpy.sign(xDiff), start[1] + index * numpy.sign(yDiff)]

                for point in ran:
                    diagram[point[1]][point[0]] += 1

            elif numpy.abs(xDiff) > 0 and numpy.abs(yDiff) == 0:
                # Horizontal line
                for x in range(start[0], target[0] + numpy.sign(xDiff), numpy.sign(xDiff)):
                    diagram[start[1]][x] += 1
            elif numpy.abs(xDiff) == 0 and numpy.abs(yDiff) > 0:
                # Vertical line
                for y in range(start[1], target[1] + numpy.sign(yDiff), numpy.sign(yDiff)):
                    diagram[y][start[0]] += 1

        howManyIsMoreThanOne = 0
        for col in diagram:
            for num in col:
                if num > 1:
                    howManyIsMoreThanOne += 1
        return howManyIsMoreThanOne
