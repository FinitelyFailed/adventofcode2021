class Octopus():

    __energy = 0
    __flashed = False

    def __init__(self, energy):
        self.__energy = energy

    def increase_energy_level(self):    
        self.__energy += 1

    def set_flashed(self):
        self.__flashed = True

    def get_flashed(self):
        return self.__flashed

    def reset(self):
        if self.__flashed:
            self.__energy = 0
            self.__flashed = False
    
    def get_energy_level(self):
        return self.__energy