import numpy

from day11.octopus import Octopus
from dayBase import DayBase

class Day11(DayBase):
    def __init__(self):
        super().__init__("11")

    def test(self):       
        return self.__test1() and self.__test2()

    def __test1(self):
        with open('day11/input/test') as test_input_file:
            testInput = test_input_file.readlines()

        with open('day11/input/test_result') as test_input_result_file:
            testInputResult = test_input_result_file.readlines()
        
        result = True

        testResult = self.__solve1(testInput)
        if testResult == int(testInputResult[0]):
            print("Test1: SUCCESS, test result: ", testResult, ", which should be: ", testInputResult)
        else:
            print("Test1: FAILED, test result: ", testResult, ", which should be: ", testInputResult)
            result = False
        return result

    def __test2(self):
        with open('day11/input/test') as test_input_file:
            testInput = test_input_file.readlines()

        with open('day11/input/test_result2') as test_input_result_file:
            testInputResult = test_input_result_file.readlines()
        
        result = True

        testResult = self.__solve2(testInput)
        if testResult == int(testInputResult[0]):
            print("Test2: SUCCESS, test result: ", testResult, ", which should be: ", testInputResult)
        else:
            print("Test2: FAILED, test result: ", testResult, ", which should be: ", testInputResult)
            result = False
        return result

    def solve1(self):
        with open('day11/input/input') as input_file:
            input = input_file.readlines()
        return self.__solve1(input)

    def solve2(self):
        with open('day11/input/input') as input_file:
            input = input_file.readlines()
        return self.__solve2(input)

    def __solve1(self, input):
        octopuses = [[] for i in range(len(input))]

        for y in range(len(input)):
            for o in input[y].strip():
                octopuses[y].append(Octopus(int(o)))

        # print("Before any steps:")
        # self.__print(octopuses)

        sumOfFlashes = 0
        for i in range(100):
            # Inc energy level in all octopuses
            for y in range(len(octopuses)):
                for x in range(len(octopuses[y])):
                    octopuses[y][x].increase_energy_level()

            sumOfFlashes += self.__flash_octopuses(octopuses)            

            for y in range(len(octopuses)):
                for x in range(len(octopuses[y])):
                    octopuses[y][x].reset()

            # print("After step" , i + 1, ":")
            # self.__print(octopuses)

        return sumOfFlashes

    def __solve2(self, input):
        octopuses = [[] for i in range(len(input))]

        for y in range(len(input)):
            for o in input[y].strip():
                octopuses[y].append(Octopus(int(o)))

        #print("Before any steps:")
        #self.__print(octopuses)

        step = 0
        sumOfFlashes = 0
        while sumOfFlashes < len(octopuses) * len(octopuses[0]):
            # Inc energy level in all octopuses
            for y in range(len(octopuses)):
                for x in range(len(octopuses[y])):
                    octopuses[y][x].increase_energy_level()


            sumOfFlashes = self.__flash_octopuses(octopuses)

            for y in range(len(octopuses)):
                for x in range(len(octopuses[y])):
                    octopuses[y][x].reset()

            # print("After step" , i + 1, ":")
            # self.__print(octopuses)
            step += 1

        return step

    def __flash_octopuses(self, octopuses) -> int:
        
        sumOfFlashes = 0
        for y in range(len(octopuses)):
            for x in range(len(octopuses[y])):
                if octopuses[y][x].get_energy_level() > 9 and not octopuses[y][x].get_flashed():
                    octopuses[y][x].set_flashed()
                    sumOfFlashes += 1

                    for iY in range(max(y - 1, 0), min(y + 2, len(octopuses))):
                        for iX in range(max(x - 1, 0), min(x + 2, len(octopuses[y]))):
                            if not (x == iX and y == iY):
                                octopuses[iY][iX].increase_energy_level()

        if sumOfFlashes > 0:
            sumOfFlashes += self.__flash_octopuses(octopuses)
        
        return sumOfFlashes

    def __print(self, octopuses):
        message = ""
        for y in range(len(octopuses)):
            for x in range(len(octopuses[y])):
                message += str(octopuses[y][x].get_energy_level())
            message += '\n'
        print(message)