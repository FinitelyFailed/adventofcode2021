import abc

class DayBase(abc.ABC):
    dayNum = 0

    def __init__(self, dayNum):
        self.dayNum = dayNum

    @classmethod
    @abc.abstractmethod
    def test(self):
        pass

    @classmethod
    @abc.abstractmethod
    def solve1(self):
        pass

    @classmethod
    @abc.abstractmethod
    def solve2(self):
        pass
