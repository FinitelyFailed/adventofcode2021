from os import initgroups
from dayBase import DayBase

from typing import Counter, Dict

class Day14(DayBase):
    def __init__(self):
        super().__init__("14")

    def test(self):       
        return self.__test1() and self.__test2()

    def __test1(self):
        with open('day14/input/test') as test_input_file:
            testInput = test_input_file.readlines()

        with open('day14/input/test_result') as test_input_result_file:
            testInputResult = test_input_result_file.readlines()
        
        result = True

        testResult = self.__solve1(testInput)
        if testResult == int(testInputResult[0]):
            print("Test1: SUCCESS, test result: ", testResult, ", which should be: ", testInputResult)
        else:
            print("Test1: FAILED, test result: ", testResult, ", which should be: ", testInputResult)
            result = False
        return result

    def __test2(self):
        with open('day14/input/test') as test_input_file:
            testInput = test_input_file.readlines()

        with open('day14/input/test_result2') as test_input_result_file:
            testInputResult = test_input_result_file.readlines()
        
        result = True

        testResult = self.__solve2(testInput)
        if testResult == int(testInputResult[0]):
            print("Test2: SUCCESS, test result: ", testResult, ", which should be: ", testInputResult)
        else:
            print("Test2: FAILED, test result: ", testResult, ", which should be: ", testInputResult)
            result = False
        return result

    def solve1(self):
        with open('day14/input/input') as input_file:
            input = input_file.readlines()
        return self.__solve1(input)

    def solve2(self):
        with open('day14/input/input') as input_file:
            input = input_file.readlines()
        return self.__solve2(input)

    def __solve1(self, input):
        intialMaterial = input[0].strip()

        insertions = {}
        for i in range(2, len(input)):
            parts = input[i].strip().split(" -> ")
            insertions[parts[0]] = parts[1]

        m = self.__hej(intialMaterial, insertions, 10)

        counter = Counter(m)
        value = counter.most_common()[0][1] - counter.most_common()[-1][1]

        return value

    def __solve2(self, input):
        intialMaterial = input[0].strip()

        insertions = {}
        for i in range(2, len(input)):
            parts = input[i].strip().split(" -> ")
            insertions[parts[0]] = parts[1]

        m = self.__hej(intialMaterial, insertions, 40)

        counter = Counter(m)
        value = counter.most_common()[0][1] - counter.most_common()[-1][1]

        return value

    def __hej(self, material: str, insertions: Dict[str, str], rounds: int) -> str:
        
        mat = material
        for round in range(rounds):
            ret = ""

            for i in range(len(mat)):
                if (i >= len(mat) - 1):
                    mat = ret + mat[i]

                ret += mat[i]
                m = mat[i:i + 2]
                if m in insertions:
                    ret += insertions[m]
        return mat