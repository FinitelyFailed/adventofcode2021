from numpy import *
import numpy

from typing import List

from numpy.core import numeric

class Paper():

    __page = [[]]
    __folds = []

    def __init__(self) -> None:
        self.__page = [['.']]

    def add_fold(self, axis: str, pos: int):
        self.__folds.append((axis, pos))

    def add_dot(self, x: int, y: int):
        if len(self.__page) <= y or len(self.__page[0]) <= x:
            self.__page = self.__resize_page(self.__page, x + 1, y + 1)            
        self.__page[y][x] = '#'

    def fold(self, numOfFolds: int):
        for i in range(numOfFolds):
            if len(self.__folds) > 0:
                fold = self.__folds.pop(0)
                self.__fold(fold[0], fold[1])

    def fold_all(self):
        self.fold(len(self.__folds))

    def __fold(self, axis: str, pos: int):    
        if axis == 'x':
            for y in range(len(self.__page)):
                for x in range(pos + 1, len(self.__page[y])):
                    if self.__page[y][x] == '#':
                        self.__page[y][(pos * 2) - x] = self.__page[y][x]
                        self.__page[y][x] = '.'
            self.__page = self.__resize_page(self.__page, pos, len(self.__page))
        elif axis == 'y':
            for y in range(pos + 1, len(self.__page)):
                for x in range(len(self.__page[y])):
                    if self.__page[y][x] == '#':
                        self.__page[(pos * 2) - y][x] = self.__page[y][x]
                        self.__page[y][x] = '.'
            self.__page = self.__resize_page(self.__page, len(self.__page[0]), pos)
        

    def get_num_of_dots(self) -> int:
        sum = 0
        for row in self.__page:
            for point in row:
                if point == '#':
                    sum += 1
        return sum
    
    def print_all(self):
        for row in self.__page:            
            print(row)

    def __get_sign_at_pos(self, page: List[List[chr]], x: int, y: int) -> char:
        if x < len(self.__page[0]) and y < len(self.__page): 
            return self.__page[y][x] 
        else: 
            return '.'

    def __resize_page(self, page: List[List[chr]], width: int, heigh: int) -> List[List[chr]]:
        if width > len(page[0]) or heigh > len(page):
            newHeight = max(len(page), heigh)
            newWidth = max(len(page[0]), width)
            newPage = [[self.__get_sign_at_pos(page, x, y) for x in range(newWidth)] for y in range(newHeight)]
            return newPage
        else:
            newPage = [[self.__get_sign_at_pos(page, x, y) for x in range(width)] for y in range(heigh)]
            return newPage