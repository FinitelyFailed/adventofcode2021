from dayBase import DayBase

from day13.paper import Paper

class Day13(DayBase):
    def __init__(self):
        super().__init__("13")

    def test(self):       
        return self.__test1() and self.__test2()

    def __test1(self):
        with open('day13/input/test') as test_input_file:
            testInput = test_input_file.readlines()

        with open('day13/input/test_result') as test_input_result_file:
            testInputResult = test_input_result_file.readlines()
        
        result = True

        testResult = self.__solve1(testInput)
        if testResult == int(testInputResult[0]):
            print("Test1: SUCCESS, test result: ", testResult, ", which should be: ", testInputResult)
        else:
            print("Test1: FAILED, test result: ", testResult, ", which should be: ", testInputResult)
            result = False
        return result

    def __test2(self):
        with open('day13/input/test') as test_input_file:
            testInput = test_input_file.readlines()

        with open('day13/input/test_result2') as test_input_result_file:
            testInputResult = test_input_result_file.readlines()
        
        result = True

        testResult = self.__solve2(testInput)
        if testResult == int(testInputResult[0]):
            print("Test2: SUCCESS, test result: ", testResult, ", which should be: ", testInputResult)
        else:
            print("Test2: FAILED, test result: ", testResult, ", which should be: ", testInputResult)
            result = False
        return result

    def solve1(self):
        with open('day13/input/input') as input_file:
            input = input_file.readlines()
        return self.__solve1(input)

    def solve2(self):
        with open('day13/input/input') as input_file:
            input = input_file.readlines()
        return self.__solve2(input)

    def __solve1(self, input):

        paper = Paper()

        createFolds = False
        for line in input:
            if not line.strip():
                createFolds = True
            else:
                if createFolds:
                    foldInput = line[11:].strip().split("=")
                    paper.add_fold(foldInput[0], int(foldInput[1]))
                else:
                    dotCoord = list(map(int, line.strip().split(",")))
                    paper.add_dot(dotCoord[0], dotCoord[1])

        paper.fold(1)

        return paper.get_num_of_dots()

    def __solve2(self, input):
        paper = Paper()

        createFolds = False
        for line in input:
            if not line.strip():
                createFolds = True
            else:
                if createFolds:
                    foldInput = line[11:].strip().split("=")
                    paper.add_fold(foldInput[0], int(foldInput[1]))
                else:
                    dotCoord = list(map(int, line.strip().split(",")))
                    paper.add_dot(dotCoord[0], dotCoord[1])

        paper.fold_all()

        paper.print_all()

        return 0
