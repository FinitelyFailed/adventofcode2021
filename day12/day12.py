from day12.cave_system import CaveSystem
from dayBase import DayBase

from typing import List
from typing import Dict
from collections import Counter

class Day12(DayBase):
    def __init__(self):
        super().__init__("12")

    def test(self):       
        return self.__test1() and self.__test2()

    def __test1(self):
        with open('day12/input/test') as test_input_file:
            testInput = test_input_file.readlines()

        with open('day12/input/test_result') as test_input_result_file:
            testInputResult = test_input_result_file.readlines()
        
        result = True

        testInputs = []
        currentTest = []
        for i in range(len(testInput)):
            if not testInput[i].strip():
                testInputs.append(currentTest)
                currentTest = []
            else:
                currentTest.append(testInput[i].strip())
        if len(currentTest) > 0:
            testInputs.append(currentTest)

        for i in range(len(testInputs)):
            testResult = self.__solve1(testInputs[i])
            expectedResult = int(testInputResult[i].strip())
            if testResult == expectedResult:
                print("Test1.",i ,": SUCCESS, test result: ", testResult, ", which should be: ", expectedResult)
            else:
                print("Test1.",i, ": FAILED, test result: ", testResult, ", which should be: ", expectedResult)
                result = False
            
        return result

    def __test2(self):
        with open('day12/input/test') as test_input_file:
            testInput = test_input_file.readlines()

        with open('day12/input/test_result2') as test_input_result_file:
            testInputResult = test_input_result_file.readlines()
        
        result = True

        testInputs = []
        currentTest = []
        for i in range(len(testInput)):
            if not testInput[i].strip():
                testInputs.append(currentTest)
                currentTest = []
            else:
                currentTest.append(testInput[i].strip())
        if len(currentTest) > 0:
            testInputs.append(currentTest)

        for i in range(len(testInputs)):
            testResult = self.__solve2(testInputs[i])
            expectedResult = int(testInputResult[i].strip())
            if testResult == expectedResult:
                print("Test2.",i, ": SUCCESS, test result: ", testResult, ", which should be: ", expectedResult)
            else:
                print("Test2.",i, ": FAILED, test result: ", testResult, ", which should be: ", expectedResult)
                result = False

        return result

    def solve1(self):
        with open('day12/input/input') as input_file:
            input = input_file.readlines()
        return self.__solve1(input)

    def solve2(self):
        with open('day12/input/input') as input_file:
            input = input_file.readlines()
        return self.__solve2(input)

    def __solve1(self, input):
        caves = self.__create_cave_system(input)
                
        paths = self.__get_paths(caves, "start", "end")

        return len(paths)

    def __solve2(self, input):
        caves = self.__create_cave_system(input)
                
        paths = self.__get_paths2(caves, "start", "end")

        return len(paths)

    def __create_cave_system(self, connections: List[str]) -> Dict[str, CaveSystem]:
        caves = {}
        for connection in connections:
            caveLabels = connection.strip().split("-")
            cave1 = self.__get_cave(caveLabels[0], caves)
            cave2 = self.__get_cave(caveLabels[1], caves)
            
            cave1.add_adjacent_cave(cave2)
            cave2.add_adjacent_cave(cave1)
        return caves

    def __get_cave(self, label: str, caves: Dict[str, CaveSystem]) -> CaveSystem:
        if not label in caves:
            cave = CaveSystem(label)
            caves[label] = cave
            return cave
        return caves[label]

    def __get_paths(self, caves: Dict[str, CaveSystem], startCave: str, endCave: str) -> List[List[str]]:
        return self.__calculate_paths(caves, startCave, [], endCave)

    def __calculate_paths(self, 
                caves: Dict[str, CaveSystem], 
                currentCaveLabel: str, 
                visitedCaves: List[str], 
                endCave: str) -> List[List[str]]:

        currentCave = caves[currentCaveLabel]

        if currentCave.is_end():
            return [visitedCaves + [currentCaveLabel]]

        ret = []
        for adjacentCave in currentCave.get_adjacent_caves():                        
            if adjacentCave.is_large_cave() or not (adjacentCave.get_label() in visitedCaves):
                vC = visitedCaves.copy()
                vC.append(currentCaveLabel)
                result = self.__calculate_paths(caves, adjacentCave.get_label(), vC, endCave)
                if len(result) > 0:
                    ret = ret + result
        return ret

    def __get_paths2(self, caves: Dict[str, CaveSystem], startCave: str, endCave: str) -> List[List[str]]:
        return self.__calculate_paths2(caves, startCave, [], endCave)

    def __calculate_paths2(self, 
                caves: Dict[str, CaveSystem], 
                currentCaveLabel: str, 
                visitedCaves: List[str], 
                endCave: str) -> List[List[str]]:

        currentCave = caves[currentCaveLabel]

        if currentCave.is_end():
            return [visitedCaves + [currentCaveLabel]]

        ret = []
        for adjacentCave in currentCave.get_adjacent_caves():                        
            vC = visitedCaves.copy()
            vC.append(currentCaveLabel)
            if self.__should_visit_cave(caves, vC):    
                result = self.__calculate_paths2(caves, adjacentCave.get_label(), vC, endCave)
                if len(result) > 0:
                    ret = ret + result
        return ret

    def __should_visit_cave(self, caves: Dict[str, CaveSystem], path: List[str]) -> bool:
        if caves[path[len(path) - 1]].is_start():
            return len(path) == 1 or (not path[len(path) - 1] in path)

        if caves[path[len(path) - 1]].is_large_cave():
            return True

        #smallCaves = [x for x in path if str.islower(x)]
        countedCaves = Counter(path)

        foundKey = False
        for key in countedCaves:
            if str.islower(key):
                if countedCaves[key] > 2:
                    return False
                if countedCaves[key] > 1:
                    if foundKey:
                        return False
                    foundKey = True
        return True
