class CaveSystem():

    __label = ''
    __isLargeCave = False
    __isStart = False
    __isEnd = False
    __adjacentCaves = []

    def __init__(self, label: str) -> None:
        self.__label = label

        if label == "start":
            self.__isStart = True
        elif label == "end":
            self.__isEnd = True
        else:
            self.__isLargeCave = str.isupper(label)
        self.__adjacentCaves = []

    def is_large_cave(self) -> bool:
        return self.__isLargeCave

    def is_start(self) -> bool:
        return self.__isStart
    
    def is_end(self) -> bool:
        return self.__isEnd

    def get_label(self) -> str:
        return self.__label

    def add_adjacent_cave(self, cave):
        if cave in self.__adjacentCaves:
            return

        if cave == self:
            return

        self.__adjacentCaves.append(cave)

    def get_adjacent_caves(self):
        return self.__adjacentCaves
