# import the pygame module, so you can use it
import pygame
import pygame_menu

from dayBase import DayBase

from day1.day1 import Day1
from day2.day2 import Day2
from day3.day3 import Day3
from day4.day4 import Day4
from day5.day5 import Day5
from day6.day6 import Day6
from day7.day7 import Day7
from day8.day8 import Day8
from day9.day9 import Day9
from day10.day10 import Day10
from day11.day11 import Day11
from day12.day12 import Day12
from day13.day13 import Day13
from day14.day14 import Day14

days = [Day1(),
    Day2(),
    Day3(),
    Day4(),
    Day5(),
    Day6(),
    Day7(),
    Day8(),
    Day9(),
    Day10(),
    Day11(),
    Day12(),
    Day13(),
    Day14()]
current_day: DayBase = days[len(days) - 1]

# define a main function
def main():
    # initialize the pygame module
    pygame.init()
    # load and set the logo
    #logo = pygame.image.load("logo32x32.png")
    #pygame.display.set_icon(logo)
    pygame.display.set_caption("Advent of code 2021")

    surface = pygame.display.set_mode((1280, 840))
     
    menu = pygame_menu.Menu('Welcome', 1280, 840,
                        theme=pygame_menu.themes.THEME_BLUE)

    menu.add.selector('Day :', list(map(lambda day: [day.dayNum, day], reversed(days))), onchange=set_day)
    menu.add.button('Test', run_tests)
    menu.add.button('Start', run_day)
    menu.add.button('Quit', pygame_menu.events.EXIT)

    menu.mainloop(surface)

def set_day(value, day):
    global current_day
    current_day = day

def run_tests():
    if current_day.test():
        print("Test was a okey!")
    else:
        print("Test fucked up! :(")

def run_day():
    print("Solution 1 of day", current_day.dayNum, ": ", current_day.solve1())
    print("Solution 2 of day", current_day.dayNum, ": ", current_day.solve2())
     
# run the main function only if this module is executed as the main script
# (if you import this as a module then nothing is executed)
if __name__=="__main__":
    # call the main function
    main()
