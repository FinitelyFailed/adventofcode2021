from dayBase import DayBase

from day10.chunk import Chunk

class Day10(DayBase):
    def __init__(self):
        super().__init__("10")

    def test(self):       
        return self.__test1() and self.__test2()

    def __test1(self):
        with open('day10/input/test') as test_input_file:
            testInput = test_input_file.readlines()

        with open('day10/input/test_result') as test_input_result_file:
            testInputResult = test_input_result_file.readlines()
        
        result = True

        testResult = self.__solve1(testInput)
        if testResult == int(testInputResult[0]):
            print("Test1: SUCCESS, test result: ", testResult, ", which should be: ", testInputResult)
        else:
            print("Test1: FAILED, test result: ", testResult, ", which should be: ", testInputResult)
            result = False
        return result

    def __test2(self):
        with open('day10/input/test') as test_input_file:
            testInput = test_input_file.readlines()

        with open('day10/input/test_result2') as test_input_result_file:
            testInputResult = test_input_result_file.readlines()
        
        result = True

        testResult = self.__solve2(testInput)
        if testResult == int(testInputResult[0]):
            print("Test2: SUCCESS, test result: ", testResult, ", which should be: ", testInputResult)
        else:
            print("Test2: FAILED, test result: ", testResult, ", which should be: ", testInputResult)
            result = False
        return result

    def solve1(self):
        with open('day10/input/input') as input_file:
            input = input_file.readlines()
        return self.__solve1(input)

    def solve2(self):
        with open('day10/input/input') as input_file:
            input = input_file.readlines()
        return self.__solve2(input)

    def __solve1(self, input):
        sum = 0
        for line in input:
            sum += self.__calculate_corrupt_score(line.strip())
        return sum

    def __solve2(self, input):
        sums = []
        for line in input:
            sum = self.__calculate_auto_complete_score(line.strip())
            if sum > 0:
                sums.append(sum)
        
        sums.sort()
        middleSum = sums[int(len(sums) / 2)]
        return middleSum

    def __calculate_corrupt_score(self, line) -> int:
        currentChunk = None
        for c in line:
            chunk = self.__construct_chunk(c)
            if chunk == None:
                if not currentChunk.end_chunk(c):
                    return self.__get_score_from_char(c)
            else:
                if currentChunk == None:
                    currentChunk = chunk
                else:
                    currentChunk.add_chunk(chunk)        
        return 0

    def __calculate_auto_complete_score(self, line) -> int:
        currentChunk = None
        for c in line:
            chunk = self.__construct_chunk(c)
            if chunk == None:
                if not currentChunk.end_chunk(c):
                    return 0
            else:
                if currentChunk == None:
                    currentChunk = chunk
                else:
                    currentChunk.add_chunk(chunk)

        endSequence = currentChunk.get_ending()        

        return self.__calculate_auto_complete_ending_score(endSequence)

    def __construct_chunk(self, char) -> Chunk:
        if char == '(':
            return Chunk('(', ')')
            return None
        if char == '[':
            return Chunk('[', ']')
            return None
        if char == '{':
            return Chunk('{', '}')
            return None
        if char == '<':
            return Chunk('<', '>')
            return None
        return None

    def __get_score_from_char(self, char) -> int:
        if char == ')':
            return 3
        if char == ']':
            return 57
        if char == '}':
            return 1197
        if char == '>':
            return 25137

    def __calculate_auto_complete_ending_score(self, ending: str) -> int:
        sum = 0
        for c in ending:
            sum *= 5
            if c == ')':
                sum += 1
            elif c == ']':
                sum += 2
            elif c == '}':
                sum += 3
            elif c == '>':
                sum += 4
        return sum