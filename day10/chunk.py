class Chunk():

    __startChar = ' '
    __endChar = ' '

    __childChunk = None

    def __init__(self, startChar, endChar):
        self.__startChar = startChar
        self.__endChar = endChar

    def add_chunk(self, chunk):
        if self.__childChunk == None:
            self.__childChunk = chunk
        else:
            self.__childChunk.add_chunk(chunk)

    def end_chunk(self, char) -> bool:
        if self.__childChunk == None:
            return char == self.__endChar

        if self.__childChunk.get_end_char() == char and self.__childChunk.is_leaf():
            self.__childChunk = None
            return True

        return self.__childChunk.end_chunk(char)

    def get_start_char(self) -> chr:
        return self.__startChar

    def get_end_char(self) -> chr:
        return self.__endChar

    def is_leaf(self) -> bool:
        return self.__childChunk == None

    def get_ending(self) -> str:
        if self.__childChunk == None:
            return self.__endChar
        return self.__childChunk.get_ending() + self.__endChar