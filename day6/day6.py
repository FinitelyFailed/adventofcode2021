from dayBase import DayBase

class Day6(DayBase):
    def __init__(self):
        super().__init__("6")

    def test(self):       
        return self.__test1() and self.__test2()

    def __test1(self):
        with open('day6/input/test') as test_input_file:
            testInput = test_input_file.readlines()

        with open('day6/input/test_result') as test_input_result_file:
            testInputResult = test_input_result_file.readlines()
        
        result = True

        testResult = self.__solve1(testInput)
        if testResult == int(testInputResult[0]):
            print("Test1: SUCCESS, test result: ", testResult, ", which should be: ", testInputResult)
        else:
            print("Test1: FAILED, test result: ", testResult, ", which should be: ", testInputResult)
            result = False
        return result

    def __test2(self):
        with open('day6/input/test') as test_input_file:
            testInput = test_input_file.readlines()

        with open('day6/input/test_result2') as test_input_result_file:
            testInputResult = test_input_result_file.readlines()
        
        result = True

        testResult = self.__solve2(testInput)
        if testResult == int(testInputResult[0]):
            print("Test2: SUCCESS, test result: ", testResult, ", which should be: ", testInputResult)
        else:
            print("Test2: FAILED, test result: ", testResult, ", which should be: ", testInputResult)
            result = False
        return result

    def solve1(self):
        with open('day6/input/input') as input_file:
            input = input_file.readlines()
        return self.__solve1(input)

    def solve2(self):
        with open('day6/input/input') as input_file:
            input = input_file.readlines()
        return self.__solve2(input)

    def __solve1(self, input):
        fishes = list(map(int, input[0].strip().split(",")))
        sum = 0

        cache = {}

        for fish in fishes:
            if fish + 1 in cache:
                sum += cache[fish + 1]
            else:
                numOfFishes = self.__calculate_fish_births(fish + 1, 80, cache)
                cache[fish + 1] = numOfFishes
                sum += numOfFishes
        return sum

    def __solve2(self, input):
        fishes = list(map(int, input[0].strip().split(",")))
        sum = 0

        cache = {}

        for fish in fishes:
            if fish + 1 in cache:
                sum += cache[fish + 1]
            else:
                numOfFishes = self.__calculate_fish_births(fish + 1, 256, cache)
                cache[fish + 1] = numOfFishes
                sum += numOfFishes
        return sum

    def __calculate_fish_births(self, birthDay, endDay, cache) -> int:
        if (birthDay in cache):
            return cache[birthDay]

        if birthDay <= endDay:
            ret = self.__calculate_fish_births(birthDay + 7, endDay, cache) + self.__calculate_fish_births(birthDay + 9, endDay, cache)
            cache[birthDay] = ret
            return ret
        else:
            return 1

        # if birthDay <= endDay:
        #     return self.__hej(birthDay + 7, endDay, cache) + self.__hej(birthDay + 9, endDay, cache)
        # return 1
