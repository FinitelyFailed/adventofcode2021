from dayBase import DayBase

class Day3(DayBase):
    def __init__(self):
        super().__init__("3")

    def test(self):       
        return self.__test1() and self.__test2()

    def __test1(self):
        with open('day3/input/test') as test_input_file:
            testInput = test_input_file.readlines()

        with open('day3/input/test_result') as test_input_result_file:
            testInputResult = test_input_result_file.readlines()
        
        result = True

        testResult = self.__solve1(testInput)
        if testResult == int(testInputResult[0]):
            print("Test1: SUCCESS, test result: ", testResult, ", which should be: ", testInputResult)
        else:
            print("Test1: FAILED, test result: ", testResult, ", which should be: ", testInputResult)
            result = False
        return result

    def __test2(self):
        with open('day3/input/test') as test_input_file:
            testInput = test_input_file.readlines()

        with open('day3/input/test_result2') as test_input_result_file:
            testInputResult = test_input_result_file.readlines()
        
        result = True

        testResult = self.__solve2(testInput)
        if len(testInputResult) > 0 and testResult == int(testInputResult[0]):
            print("Test2: SUCCESS, test result: ", testResult, ", which should be: ", testInputResult)
        else:
            print("Test2: FAILED, test result: ", testResult, ", which should be: ", testInputResult)
            result = False
        return result

    def solve1(self):
        with open('day3/input/input') as input_file:
            input = input_file.readlines()
        return self.__solve1(input)

    def solve2(self):
        with open('day3/input/input') as input_file:
            input = input_file.readlines()
        return self.__solve2(input)

    def __solve1(self, input):

        numberOfOnesAtIndex = [0] * len(input[0].strip())

        for line in input:          
            for bitIndex in range(len(line)):
                if line[bitIndex] == "0":
                    numberOfOnesAtIndex[bitIndex] -= 1
                elif line[bitIndex] == "1":
                    numberOfOnesAtIndex[bitIndex] += 1

        gammaRate = 0
        epsilonRate = 0
        for index in range(len(numberOfOnesAtIndex)):
            if numberOfOnesAtIndex[index] > 0:
                gammaRate += 1 << len(numberOfOnesAtIndex) - 1 - index
            elif numberOfOnesAtIndex[index] < 0:
                epsilonRate += 1 << len(numberOfOnesAtIndex) - 1 - index

        return gammaRate * epsilonRate

    def __solve2(self, input):

        lengthOfNum = len(input[0].strip())

        inputNumbers = list(map(lambda i: int(i, 2), input))

        oxygenGeneratorRating = self.__findValueFittingMask(True, inputNumbers, lengthOfNum - 1)
        co2ScrubberRating = self.__findValueFittingMask(False, inputNumbers, lengthOfNum - 1)

        return oxygenGeneratorRating * co2ScrubberRating


    def __findValueFittingMask(self, mostCommon, inputNumbers, index):
        if len(inputNumbers) == 1:
            return inputNumbers[0]

        numOfOnesAtIndex = 0
        for num in inputNumbers:
            if num & 1 << index:
                numOfOnesAtIndex += 1

        shouldBeOne = False
        if mostCommon and numOfOnesAtIndex >= (len(inputNumbers) / 2):
            shouldBeOne = True
        elif (not mostCommon) and numOfOnesAtIndex < (len(inputNumbers) / 2):
            shouldBeOne = True

        ret = []
        for num in inputNumbers:            
            if num & 1 << index:
                if shouldBeOne:
                    ret.append(num)
            else:
                if not shouldBeOne:
                    ret.append(num)
        return self.__findValueFittingMask(mostCommon, ret, index - 1)
        