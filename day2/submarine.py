class Submarine():

    __horizontal_pos = 0
    __depth = 0
    __aim = 0
    __useAim = False

    def __init__(self, useAim: bool):
        self.__useAim = useAim

    def move(self, instruction):
        instInParts = instruction.split()
        direction = instInParts[0]
        magnitude = int(instInParts[1])

        if (direction == "forward"):        
            self.__horizontal_pos += magnitude
            if (self.__useAim):
                self.__depth += magnitude * self.__aim                
        elif (direction == "down"):            
            if (self.__useAim):
                self.__aim += magnitude
            else:
                self.__depth += magnitude
        elif (direction == "up"):            
            if (self.__useAim):
                self.__aim -= magnitude
            else:
                self.__depth -= magnitude

    def get_horizontal_pos(self):
        return self.__horizontal_pos

    def get_depth(self):
        return self.__depth