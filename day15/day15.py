from dayBase import DayBase

from day15.node import Node

from typing import List

class Day15(DayBase):
    def __init__(self):
        super().__init__("15")

    def test(self):       
        return self.__test1() and self.__test2()

    def __test1(self):
        with open('day15/input/test') as test_input_file:
            testInput = test_input_file.readlines()

        with open('day15/input/test_result') as test_input_result_file:
            testInputResult = test_input_result_file.readlines()
        
        result = True

        testResult = self.__solve1(testInput)
        if testResult == int(testInputResult[0]):
            print("Test1: SUCCESS, test result: ", testResult, ", which should be: ", testInputResult)
        else:
            print("Test1: FAILED, test result: ", testResult, ", which should be: ", testInputResult)
            result = False
        return result

    def __test2(self):
        with open('day15/input/test') as test_input_file:
            testInput = test_input_file.readlines()

        with open('day15/input/test_result2') as test_input_result_file:
            testInputResult = test_input_result_file.readlines()
        
        result = True

        testResult = self.__solve2(testInput)
        if testResult == int(testInputResult[0]):
            print("Test2: SUCCESS, test result: ", testResult, ", which should be: ", testInputResult)
        else:
            print("Test2: FAILED, test result: ", testResult, ", which should be: ", testInputResult)
            result = False
        return result

    def solve1(self):
        with open('day15/input/input') as input_file:
            input = input_file.readlines()
        return self.__solve1(input)

    def solve2(self):
        with open('day15/input/input') as input_file:
            input = input_file.readlines()
        return self.__solve2(input)

    def __solve1(self, input):

        data = []
        for y in range(len(input)):
            data.append(list(map(int, input[y].strip())))


        startX = 0
        startY = 0
        endX = len(data[0]) - 1
        endY = len(data) - 1

        startNode = None
        endNode = None

        unvistedNodes = [[None for i in range(len(data[0]))] for i in range(len(data))]
        for y in range(len(data)):
            for x in range(len(data[y])):
                node = Node(data[y][x], x, y)

                if x > 0 and unvistedNodes[x - 1][y] != None:
                    node.add_neighbour(unvistedNodes[x - 1][y])
                if y > 0 and unvistedNodes[x][y - 1] != None:
                    node.add_neighbour(unvistedNodes[x][y - 1])
                if x < len(data[y]) - 1 and unvistedNodes[x + 1][y] != None:
                    node.add_neighbour(unvistedNodes[x + 1][y])
                if x < len(data) - 1 and unvistedNodes[x][y + 1] != None:
                    node.add_neighbour(unvistedNodes[x][y + 1])                

                if x == startX and y == startY:
                    startNode = node                    
                elif x == endX and y == endY:
                    endNode = node
                    unvistedNodes[y][x] = node
                else:
                    unvistedNodes[y][x] = node                

        currentNode = startNode
        for neighbour in currentNode.get_neighbours:
            if not neighbour.is_visisted():
                cost = neighbour.get_weight() + currentNode.cost
                if cost < neighbour.get_cost():
                    neighbour.set_cost(cost)
        currentNode.set_visited()
        unvisitedSet.remove(currentNode)

        currentNode = self.__get_cheapest_node(unvistedNodes)

        return 0

    def __solve2(self, input):
        return 0


    def __get_cheapest_node(self, unvisited: List[List[Node]]) -> Node:
        cheapestNode = None

        for y in len(unvisited):
            for x in len(unvisited[y]):
                if 
                if cheapestNode == None or cheapestNode.get_cost() > unvisited[y][x].get_cost():
                    cheapestNode = unvisited[y][x]
        return node

    def __remove_node_from_unvisited_nodes(self, unvisited: List[List[Node]], node: Node):
