import sys

class Node():

    __weight = 0
    __is_visited = False
    __cost = sys.maxsize
    __neighbours = []
    __x = -1
    __y = -1

    def __init__(self, weight: int, x: int, y: int) -> None:
        self.__weight = weight
        self.__x = x
        self.__y = y
        self.__neighbours = []

    def get_weight(self) -> int:
        return self.__weight

    def get_cost(self) -> int:
        return self.__cost

    def set_cost(self, cost: int):
        self.__cost = cost

    def is_visited(self) -> bool:
        return self.__is_visited

    def set_visisted(self):
        self.__is_visited = True

    def add_neighbour(self, node):
        self.__neighbours.append(node)

    def get_x(self) -> int:
        return self.__x

    def get_y(self) -> int:
        return self.__y