from day4.number import Number

class BingoBoard():

    __nums = [[]]
    __score = 0

    def __init__(self, numbers):

        self.__nums = [[]] * len(numbers)
        for rowIndex in range(len(numbers)):
            row = []
            for num in list(map(int, numbers[rowIndex].strip().split())):
                row.append(Number(num))
            self.__nums[rowIndex] = row
        
    def mark(self, num):
        for row in self.__nums:
            for number in row:
                if number.get_number() == num:
                    number.set_marked()
                    if self.__check_if_winner():
                        self.__score = self.__calculate_score(num)

    def get_score(self):
        return self.__score

    def is_winner(self):
        return self.__score > 0

    def __check_if_winner(self):
        winnerColumns = [True] * len(self.__nums[0])

        for row in range(len(self.__nums)):
            winnerRow = True
            for col in range(len(self.__nums[row])):
                if not self.__nums[row][col].get_marked():
                    winnerRow = False
                    winnerColumns[col] = False
            if winnerRow:
                return True

        for isColWinner in winnerColumns:
            if isColWinner:
                return True
        return False

    def __calculate_score(self, num):
        sum = 0

        for row in self.__nums:
            for number in row:
                if not number.get_marked():
                    sum += number.get_number()

        return sum * num