from day4 import bingoBoard
from day4.bingoBoard import BingoBoard

class Bingo():
    __numbersToDraw = []
    __boards = []

    def __init__(self, input):
        self.__numbersToDraw = self.__getNumbersToDraw(input[0])
        self.__boards = self.__createBoards(input[2:len(input)])

    def runUntilWinner(self):
        for num in self.__numbersToDraw:
            for board in self.__boards:
                board.mark(num)

                if board.is_winner():
                    return board.get_score()
        return 0

    def runAllInputNumbers(self):
        sortedWinners = []

        for num in self.__numbersToDraw:
            for board in self.__boards:
                if not board.is_winner():
                    board.mark(num)

                    if board.is_winner():
                        sortedWinners.append(board)

        return sortedWinners

    def __getNumbersToDraw(self, input):
        return list(map(int, input.split(",")))

    def __createBoards(self, input):        
        ret = []
        boardNumbers = []

        for line in input:
            if not line.strip():
                ret.append(BingoBoard(boardNumbers))
                boardNumbers = []
            else:
                boardNumbers.append(line)

        if boardNumbers:
            ret.append(BingoBoard(boardNumbers))

        return ret