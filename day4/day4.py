from dayBase import DayBase
from day4.bingo import Bingo

class Day4(DayBase):
    def __init__(self):
        super().__init__("4")

    def test(self):       
        return self.__test1() and self.__test2()

    def __test1(self):
        with open('day4/input/test') as test_input_file:
            testInput = test_input_file.readlines()

        with open('day4/input/test_result') as test_input_result_file:
            testInputResult = test_input_result_file.readlines()
        
        result = True

        testResult = self.__solve1(testInput)
        if testResult == int(testInputResult[0]):
            print("Test1: SUCCESS, test result: ", testResult, ", which should be: ", testInputResult)
        else:
            print("Test1: FAILED, test result: ", testResult, ", which should be: ", testInputResult)
            result = False
        return result

    def __test2(self):
        with open('day4/input/test') as test_input_file:
            testInput = test_input_file.readlines()

        with open('day4/input/test_result2') as test_input_result_file:
            testInputResult = test_input_result_file.readlines()
        
        result = True

        testResult = self.__solve2(testInput)
        if len(testInputResult) > 0 and testResult == int(testInputResult[0]):
            print("Test2: SUCCESS, test result: ", testResult, ", which should be: ", testInputResult)
        else:
            print("Test2: FAILED, test result: ", testResult, ", which should be: ", testInputResult)
            result = False
        return result

    def solve1(self):
        with open('day4/input/input') as input_file:
            input = input_file.readlines()
        return self.__solve1(input)

    def solve2(self):
        with open('day4/input/input') as input_file:
            input = input_file.readlines()
        return self.__solve2(input)

    def __solve1(self, input):
        bingo = Bingo(input)

        scoreOfWinner = bingo.runUntilWinner()

        return scoreOfWinner

    def __solve2(self, input):
        bingo = Bingo(input)

        sortedWinners = bingo.runAllInputNumbers()

        loserScore = sortedWinners[len(sortedWinners) - 1].get_score()

        return loserScore
