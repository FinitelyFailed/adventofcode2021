class Number():
    
    __number = 0
    __marked = False

    def __init__(self, number):
        self.__number = number

    def set_marked(self):
        self.__marked = True

    def get_marked(self):
        return self.__marked

    def get_number(self):
        return self.__number