from dayBase import DayBase

from math import *
from numpy import *
import numpy
import sys

class Day7(DayBase):
    def __init__(self):
        super().__init__("7")

    def test(self):       
        return self.__test1() and self.__test2()

    def __test1(self):
        with open('day7/input/test') as test_input_file:
            testInput = test_input_file.readlines()

        with open('day7/input/test_result') as test_input_result_file:
            testInputResult = test_input_result_file.readlines()
        
        result = True

        testResult = self.__solve1(testInput)
        if testResult == int(testInputResult[0]):
            print("Test1: SUCCESS, test result: ", testResult, ", which should be: ", testInputResult)
        else:
            print("Test1: FAILED, test result: ", testResult, ", which should be: ", testInputResult)
            result = False
        return result

    def __test2(self):
        with open('day7/input/test') as test_input_file:
            testInput = test_input_file.readlines()

        with open('day7/input/test_result2') as test_input_result_file:
            testInputResult = test_input_result_file.readlines()
        
        result = True

        testResult = self.__solve2(testInput)
        if testResult == int(testInputResult[0]):
            print("Test2: SUCCESS, test result: ", testResult, ", which should be: ", testInputResult)
        else:
            print("Test2: FAILED, test result: ", testResult, ", which should be: ", testInputResult)
            result = False
        return result

    def solve1(self):
        with open('day7/input/input') as input_file:
            input = input_file.readlines()
        return self.__solve1(input)

    def solve2(self):
        with open('day7/input/input') as input_file:
            input = input_file.readlines()
        return self.__solve2(input)

    def __solve1(self, input):
        crabs = list(map(int, input[0].strip().split(",")))
        crabs.sort()

        median = 0
        if len(crabs) % 2 == 0:
            xIndex = len(crabs) / 2 - 1
            yIndex = len(crabs) / 2

            x = crabs[int(xIndex)]
            y = crabs[int(yIndex)]

            median = int((x + y) / 2)
        else:
            index = len(crabs) / 2 - 1

            median = crabs[int(index)]

        sum = 0
        for crab in crabs:
            sum += int(numpy.abs(crab - median))

        return sum

    def __solve2(self, input):
        crabs = list(map(int, input[0].strip().split(",")))
        crabs.sort()

        rangeOfAllPositions = range(crabs[0], crabs[len(crabs) - 1])
        totalFuel = [0 for i in rangeOfAllPositions]

        cache = {}        

        for i in range(len(crabs)):
            for j in rangeOfAllPositions:
                dist = int(numpy.abs(crabs[i] - j))
                if dist in cache:
                    totalFuel[j] += cache[dist]
                else:
                    fuel = self.__calculate_fuel_useage(dist)
                    cache[dist] = fuel
                    totalFuel[j] += fuel
        
        minFuel = sys.maxsize
        for fuelCost in totalFuel:
            if fuelCost < minFuel:
                minFuel = fuelCost

        return minFuel

    def __calculate_fuel_useage(self, dist):
        cost = 0
        for i in range(1, dist + 1):
            cost += i
        return cost
