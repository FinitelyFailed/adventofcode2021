from dayBase import DayBase

class Day9(DayBase):
    def __init__(self):
        super().__init__("9")

    def test(self):       
        return self.__test1() and self.__test2()

    def __test1(self):
        with open('day9/input/test') as test_input_file:
            testInput = test_input_file.readlines()

        with open('day9/input/test_result') as test_input_result_file:
            testInputResult = test_input_result_file.readlines()
        
        result = True

        testResult = self.__solve1(testInput)
        if testResult == int(testInputResult[0]):
            print("Test1: SUCCESS, test result: ", testResult, ", which should be: ", testInputResult)
        else:
            print("Test1: FAILED, test result: ", testResult, ", which should be: ", testInputResult)
            result = False
        return result

    def __test2(self):
        with open('day9/input/test') as test_input_file:
            testInput = test_input_file.readlines()

        with open('day9/input/test_result2') as test_input_result_file:
            testInputResult = test_input_result_file.readlines()
        
        result = True

        testResult = self.__solve2(testInput)
        if testResult == int(testInputResult[0]):
            print("Test2: SUCCESS, test result: ", testResult, ", which should be: ", testInputResult)
        else:
            print("Test2: FAILED, test result: ", testResult, ", which should be: ", testInputResult)
            result = False
        return result

    def solve1(self):
        with open('day9/input/input') as input_file:
            input = input_file.readlines()
        return self.__solve1(input)

    def solve2(self):
        with open('day9/input/input') as input_file:
            input = input_file.readlines()
        return self.__solve2(input)

    def __solve1(self, input):
        heightMap = self.__create_heightmap(input)

        sum = 0

        for x in range(len(heightMap)):
            for y in range(len(heightMap[x])):
                if self.__is_lowest_point(heightMap, x, y):
                    sum += heightMap[x][y] + 1

        return sum

    def __solve2(self, input):
        heightMap = self.__create_heightmap(input)

        lowestPointsPos = self.__find_all_pos_of_lowest_points(heightMap)

        basins = []

        for pos in lowestPointsPos:
            basins.append(self.__find_all_low_neighbors(heightMap, [], pos[0], pos[1]))

        basins.sort(key=len)

        product = len(basins[len(basins) - 1]) * len(basins[len(basins) - 2]) * len(basins[len(basins) - 3])

        return product

    def __create_heightmap(self, input):
        heightMap = [[] for i in range(len(input))]

        for i in range(len(input)):
            heightMap[i] = list(map(int, input[i].strip()))

        return heightMap

    def __is_lowest_point(self, heightMap, x, y):
        if x > 0 and heightMap[x - 1][y] <= heightMap[x][y]:
            return False

        if x < len(heightMap) - 1 and heightMap[x + 1][y] <= heightMap[x][y]:
            return False

        if y > 0 and heightMap[x][y - 1] <= heightMap[x][y]:
            return False

        if y < len(heightMap[x]) - 1 and heightMap[x][y + 1] <= heightMap[x][y]:
            return False

        return True
    
    def __find_all_pos_of_lowest_points(self, heightMap):
        lowestPointsPos = []

        for x in range(len(heightMap)):
            for y in range(len(heightMap[x])):
                if self.__is_lowest_point(heightMap, x, y):
                    lowestPointsPos.append([x,y])
        return lowestPointsPos

    def __find_all_low_neighbors(self, heightMap, positions, x, y):
        if heightMap[x][y] >= 9:
            return positions

        for pos in positions:
            if pos[0] == x and pos[1] == y:
                return positions

        ret = positions
        ret.append([x, y])

        if x > 0:
            ret = self.__find_all_low_neighbors(heightMap, ret, x - 1, y)
        if x < len(heightMap) - 1:
            ret = self.__find_all_low_neighbors(heightMap, ret, x + 1, y)
        if y > 0:
            ret = self.__find_all_low_neighbors(heightMap, ret, x, y - 1)
        if y < len(heightMap[x]) - 1:
            ret = self.__find_all_low_neighbors(heightMap, ret, x, y + 1)

        return ret