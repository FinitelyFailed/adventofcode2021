from dayBase import DayBase

class Day8(DayBase):
    def __init__(self):
        super().__init__("8")

    def test(self):       
        return self.__test1() and self.__test2()

    def __test1(self):
        with open('day8/input/test') as test_input_file:
            testInput = test_input_file.readlines()

        with open('day8/input/test_result') as test_input_result_file:
            testInputResult = test_input_result_file.readlines()
        
        result = True

        testResult = self.__solve1(testInput)
        if testResult == int(testInputResult[0]):
            print("Test1: SUCCESS, test result: ", testResult, ", which should be: ", testInputResult)
        else:
            print("Test1: FAILED, test result: ", testResult, ", which should be: ", testInputResult)
            result = False
        return result

    def __test2(self):
        with open('day8/input/test') as test_input_file:
            testInput = test_input_file.readlines()

        with open('day8/input/test_result2') as test_input_result_file:
            testInputResult = test_input_result_file.readlines()
        
        result = True

        testResult = self.__solve2(testInput)
        if testResult == int(testInputResult[0]):
            print("Test2: SUCCESS, test result: ", testResult, ", which should be: ", testInputResult)
        else:
            print("Test2: FAILED, test result: ", testResult, ", which should be: ", testInputResult)
            result = False
        return result

    def solve1(self):
        with open('day8/input/input') as input_file:
            input = input_file.readlines()
        return self.__solve1(input)

    def solve2(self):
        with open('day8/input/input') as input_file:
            input = input_file.readlines()
        return self.__solve2(input)

    def __solve1(self, input):
        numberOfDigits = [0 for i in range(10)]

        for line in input:
            parts = line.strip().split("|")
            patterns = parts[0].split() + parts[1].split()
            outputValues = parts[1].split()

            segmentsToNumberMapping = self.__get_segments_to_number_mapping(patterns)

            for value in outputValues:
                for seg in segmentsToNumberMapping:
                    if self.__compare(value, seg):
                        numberOfDigits[segmentsToNumberMapping[seg]] += 1            

        return numberOfDigits[1] + numberOfDigits[4] + numberOfDigits[7] + numberOfDigits[8]

    def __solve2(self, input):
        sum = 0

        for line in input:
            parts = line.strip().split("|")
            patterns = parts[0].split() + parts[1].split()
            outputValues = parts[1].split()

            segmentsToNumberMapping = self.__get_segments_to_number_mapping(patterns)

            outputsum = 0
            for i in range(len(outputValues)):
                for seg in segmentsToNumberMapping:
                    if self.__compare(outputValues[i], seg):
                        outputsum += segmentsToNumberMapping[seg] * pow(10, len(outputValues) - i - 1)
            sum += outputsum

        return sum

    def __get_segments_to_number_mapping(self, digits):
        numberToSegmentsMapping = ["" for i in range(10)]

        numberToSegmentsMapping[1] = self.__get_on_length(digits, 2)[0]
        numberToSegmentsMapping[4] = self.__get_on_length(digits, 4)[0]
        numberToSegmentsMapping[7] = self.__get_on_length(digits, 3)[0]
        numberToSegmentsMapping[8] = self.__get_on_length(digits, 7)[0]

        segments = [0 for i in range(7)]

        # Find segment 0, using number 1 and 7
        for c in numberToSegmentsMapping[7]:
            if not c in numberToSegmentsMapping[1]:
                segments[0] = c
                break

        # Find segment 5, using 3, 7 and 4
        # Map 3.
        for digit in digits:
            if len(digit) == 5:
                diff3To7 = self.__get_diff(digit, numberToSegmentsMapping[7])            
                if len(diff3To7) ==  2:
                    numberToSegmentsMapping[3] = digit
                    union = self.__get_union(diff3To7, numberToSegmentsMapping[4])
                    segments[5] = union
                    break
        
        # Find seg 1, using 4 and 1 and seg 5
        diff = self.__get_diff(numberToSegmentsMapping[1], numberToSegmentsMapping[4])
        segments[1] = diff.replace(segments[5], '')

        # Find segment 6, using 3
        # Map 5
        for digit in digits:
            if len(digit) < 7:
                diff = self.__get_diff(digit, numberToSegmentsMapping[3])
                if len(diff) == 2 and self.__contains_segment(diff, segments[1]):
                    numberToSegmentsMapping[5] = digit
                    segments[6] = self.__get_diff(diff, segments[1])
                    break

        segments[4] = self.__get_diff(numberToSegmentsMapping[1], segments[6])

        # Find segment 3, using a all other segments to detect 9
        for digit in digits:
            if len(digit) == 6:
                diff = self.__get_diff(digit, segments[0] + segments[1] + segments[6] + segments[5] + segments[4])
                if len(diff) == 1:
                    numberToSegmentsMapping[9] = digit
                    segments[3] = diff
                    break

        # Find segment 2, using number 8 and all other segments
        segments[2] = self.__get_diff(numberToSegmentsMapping[8], segments[0] + segments[1] + segments[3] + segments[4] + segments[5] + segments[6])

        # Find 0, 2 and 6
        for digit in digits:
            if not digit in numberToSegmentsMapping:
                if self.__compare(digit, segments[0] + segments[1] + segments[2] + segments[3] + segments[4] + segments[6]):
                    numberToSegmentsMapping[0] = digit                    
                elif self.__compare(digit, segments[0] + segments[2] + segments[3] + segments[5] + segments[6]):
                    numberToSegmentsMapping[2] = digit
                elif self.__compare(digit, segments[0] + segments[1] + segments[2] + segments[3] + segments[4] + segments[5]):
                    numberToSegmentsMapping[6] = digit

        #   0000
        #  1    6
        #  1    6
        #   5555
        #  2    4
        #  2    4
        #   3333

        #     ####             ####    ####              ####    ####  ####     ####     ####
        #    #    #     #          #       #   #    #   #       #          #   #    #   #    #
        #    #    #     #          #       #   #    #   #       #          #   #    #   #    #
        #                      ####    ####     ####     ####    ####           ####     ####
        #    #    #     #     #            #        #        #  #    #     #   #    #        #
        #    #    #     #     #            #        #        #  #    #     #   #    #        #
        #     ####             ####    ####              ####    ####           ####     ####

        # 0, 6 seg
        # 1, 2 seg
        # 2, 5 seg
        # 3, 5 seg
        # 4, 4 seg
        # 5, 5 seg
        # 6, 6 seg
        # 7, 3 seg
        # 8, 7 seg
        # 9, 6 seg

        segmentsToNumberMapping = {}
        for i in range(len(numberToSegmentsMapping)):
            segmentsToNumberMapping[numberToSegmentsMapping[i]] = i

        return segmentsToNumberMapping

    def __compare(self, digit1, digit2):
        if not len(digit1) == len(digit2):
            return False

        for c in digit1:
            if not c in digit2:
                return False
        return True

    def __contains_segment(self, digit, segment):
        return segment in digit

    def __get_diff(self, digit1, digit2):
        diff = ""
        for c in digit1:
            if not c in digit2:
                diff += c

        for c in digit2:
            if not c in digit1:
                diff += c
        
        return diff

    def __get_union(self, digit1, digit2):
        union = ""
        for c in digit1:
            if c in digit2:
                union += c
        return union

    def __get_on_length(self, digits, length):
        ret = []
        for digit in digits:
            if len(digit) == length:
                ret.append(digit)
        return ret
